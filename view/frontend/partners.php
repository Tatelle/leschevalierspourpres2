<?php $title = 'Nos Partenaires - Les Chevaliers Pourpres'; ?>

<?php ob_start(); ?>

<h1>Nos Partenaires</h1>

<h2>Associations Médiévales</h2>

<ul class="liste_partenaires">
	<li>
		<h3><img src="./public/images/partners/cia.png" alt="Logo Centre de l'Imaginaire Arthurien" />Centre de l'Imaginaire Arthurien</h3>
		<ul class="infos_partner">
			<li>Le Centre de l’Imaginaire Arthurien vous attend dans la forêt de Brocéliande pour vous conter les hauts faits du roi Arthur et de ses chevaliers.</li>
			<li><a href="mailto:contact@centre-arthurien-broceliande.com"><i class="fas fa-envelope"></i> contact@centre-arthurien-broceliande.com</a></li>
			<li><a href="http://www.centre-arthurien-broceliande.com" target="_BLANK"><i class="fas fa-globe"></i> Accéder au site</a></li>
			<li><a href="https://www.facebook.com/pg/Centrearthurien/" target="_BLANK"><i class="fab fa-facebook-square"></i> Accéder à la page Facebook</a></li>
		</ul>
	</li>

	<li>
		<h3><img src="./public/images/partners/bec.jpg" alt="Logo Brouage en Costume Passion" />Brouage en Costume Passion</h3>
		<ul class="infos_partner">
			<li>L'association Brouage En Costume Passion, fondée le 7 octobre 2010, a pour but d'animer la place-forte de Brouage, par des manifestations historiques.</li>
			<li><a href="mailto:brouagepassionanimation@gmail.com"><i class="fas fa-envelope"></i> brouagepassionanimation@gmail.com</a></li>
			<li><a href="http://www.brouage-en-costume-passion.fr" target="_BLANK"><i class="fas fa-globe"></i> Accéder au site</a></li>
			<li><a href="https://www.facebook.com/BrouageEnCostumePassion/" target="_BLANK"><i class="fab fa-facebook-square"></i> Accéder à la page Facebook</a></li>
		</ul>
	</li>

	<li>
		<h3><img src="./public/images/partners/grant_compaigne.png" alt="Logo La Grant Compaigne" /> La Grant Compaigne</h3>
		<ul class="infos_partner">
			<li>Association de reconstitution médiévale 1360-1390.
			Routiers de la guerre de Cent Ans, soudards et civils.</li>
			<li><a href="mailto:info@la-grant-compaigne.com"><i class="fas fa-envelope"></i> info@la-grant-compaigne.com</a></li>
			<li><a href="http://www.la-grant-compaigne.com/" target="_BLANK"><i class="fas fa-globe"></i> Accéder au site</a></li>
			<li><a href="https://www.facebook.com/La-Grant-Compaigne-725089030944449/" target="_BLANK"><i class="fab fa-facebook-square"></i> Accéder à la page Facebook</a></li>
		</ul>		
	</li>

	<li>
		<h3><img src="./public/images/partners/tard-venus.jpg" alt="Logo Les Tard-Venus" />Les Tard-Venus</h3>
		<ul class="infos_partner">
			<li>Association de reconstitution médiévale d'une troupe de routiers agissant à la fin du XIVeme siècle (1360-1415).</li>
			<li><a href="mailto:contact@les-tard-venus.fr"><i class="fas fa-envelope"></i> contact@les-tard-venus.fr</a></li>
			<li><a href="http://www.les-tard-venus.fr/" target="_BLANK"><i class="fas fa-globe"></i> Accéder au site</a></li>
			<li><a href="https://www.facebook.com/TardVenus/" target="_BLANK"><i class="fab fa-facebook-square"></i> Accéder à la page Facebook</a></li>
		</ul>
	</li>

	<li>
		<h3><img src="./public/images/partners/compagnie_grise.jpg" alt="Logo La Compagnie Grise" />La Compagnie Grise</h3>
		<ul class="infos_partner">
			<li>Association dont le but est de recréer des ambiances (fantastique, médiéval, arthurien, Terre du Milieu) en associant spectacle vivant et action.</li>
			<li><a href="mailto:compagnie-grise@orange.fr"><i class="fas fa-envelope"></i> compagnie-grise@orange.fr</a></li>
			<!-- <li><a href="" target="_BLANK"><i class="fas fa-globe"></i> </a></li> -->
			<li><a href="https://www.facebook.com/lacompagniegrise/" target="_BLANK"><i class="fab fa-facebook-square"></i> Accéder à la page Facebook</a></li>
		</ul>		
	</li>

	<li>
		<h3><img src="./public/images/partners/em17.jpg" alt="Logo EscriMédiévale17" />EscriMédiévale17</h3>
		<ul class="infos_partner">
			<li>Nous pratiquons l’escrime germanique selon Joachim Meyer : Épée longue, Dussack, rapière, Hast et Dague, uniquement dans un cadre purement technique. Nous sommes situés au Gymnase n°3 - complexe sportif - Surgères</li>
			<li><a href="mailto:escrimemedievale17@live.fr"><i class="fas fa-envelope"></i> escrimemedievale17@live.fr</a></li>
			<li><a href="http://www.escrimedievale17.fr/" target="_BLANK"><i class="fas fa-globe"></i> Accéder au site</a></li>
			<li><a href="https://www.facebook.com/escrimedievale17/" target="_BLANK"><i class="fab fa-facebook-square"></i> Accéder à la page Facebook</a></li>
		</ul>		
	</li>

	<li>
		<h3><img src="./public/images/partners/em22.jpg" alt="Logo EscriMédiévale22" />EscriMédiévale22</h3>
		<ul class="infos_partner">
			<li>Association loi 1901 ayant vu le jour en Janvier 2018, EscriMédiévale 22 a pour but de promouvoir les Arts Martiaux Historiques Européens dans le département des Côtes-d’Armor.</li>
			<li><a href="mailto:escrimedievale22@gmail.com"><i class="fas fa-envelope"></i> escrimedievale22@gmail.com</a></li>
			<li><a href="https://escrimedievale22.wixsite.com/em22" target="_BLANK"><i class="fas fa-globe"></i> Accéder au site</a></li>
			<li><a href="https://www.facebook.com/pg/escrimedievale22/" target="_BLANK"><i class="fab fa-facebook-square"></i> Accéder à la page Facebook</a></li>
		</ul>		
	</li>

	<li>
		<h3><img src="./public/images/partners/faits_d_armes.jpg" alt="Logo Faits d'armes" />Faits d'armes</h3>
		<ul class="infos_partner">
			<li>Equipement et protections adaptés aux Arts Martiaux Historiques Européens : Masque d'escrime, gants, épées et boucliers. Spécialiste français des AMHE.</li>
			<li><a href="mailto:renaud@faitsdarmes.com"><i class="fas fa-envelope"></i> renaud@faitsdarmes.com</a></li>
			<li><a href="https://www.faitsdarmes.com/" target="_BLANK"><i class="fas fa-globe"></i> Accéder au site</a></li>
			<li><a href="https://www.facebook.com/Faitsdarmes/" target="_BLANK"><i class="fab fa-facebook-square"></i> Accéder à la page Facebook</a></li>
		</ul>		
	</li>

	<!-- EM79 ?
	<li>
		<h3><img src="./public/images/partners/" alt="Logo " /></h3>
		<ul class="infos_partner">
			<li></li>
			<li><a href="mailto:"><i class="fas fa-envelope"></i> </a></li>
			<li><a href="" target="_BLANK"><i class="fas fa-globe"></i> </a></li>
			<li><a href="" target="_BLANK"><i class="fab fa-facebook-square"></i> </a></li>
		</ul>		
	</li> -->
</ul>

<h2>Châteaux</h2>

<ul class="liste_partenaires">
	<li>
		<h3><img src="./public/images/partners/coudray.jpg" alt="Logo Château du Coudray-Salbart" />Château du Coudray-Salbart</h3>
		<ul class="infos_partner">
			<li>Le château du Coudray-Salbart est un château français du situé sur la commune d'Échiré, dans les Deux-Sèvres.</li>
			<li><i class="fas fa-phone"></i> 05 49 25 71 07</li>
			<li><a href="mailto:contact@coudraysalbart.fr"><i class="fas fa-envelope"></i> contact@coudraysalbart.fr</a></li>
			<li><a href="http://www.coudraysalbart.fr/" target="_BLANK"><i class="fas fa-globe"></i> Accéder au site</a></li>
			<li><a href="https://www.facebook.com/AssociationDesAmisDuChateauDuCoudraySalbart/" target="_BLANK"><i class="fab fa-facebook-square"></i> Accéder à la page Facebook</a></li>
		</ul>		
	</li>

	<li>
		<h3><img src="./public/images/partners/crazannes.jpg" alt="Logo Château de Crazannes - Château du Chat Botté" />Château de Crazannes - Château du Chat Botté</h3>
		<ul class="infos_partner">
			<li>Le château de Crazannes vous propose des visites guidées, des animations, des hébergements et vous accueille pour votre mariage ou d'autres événements ...</li>
			<li><i class="fas fa-phone"></i> 05 46 90 15 94</li>
			<li><a href="mailto:contact@crazannes.com"><i class="fas fa-envelope"></i> contact@crazannes.com</a></li>
			<li><a href="http://www.crazannes.com/" target="_BLANK"><i class="fas fa-globe"></i> Accéder au site</a></li>
			<li><a href="https://www.facebook.com/chateaudecrazannes17/" target="_BLANK"><i class="fab fa-facebook-square"></i> Accéder à la page Facebook</a></li>
		</ul>		
	</li>

	<li>
		<h3><img src="./public/images/partners/roche_courbon.jpg" alt="Logo Château de la Roche Courbon" />Château de la Roche Courbon</h3>
		<ul class="infos_partner">
			<li>Monument historique - Jardins remarquables</li>
			<li><i class="fas fa-phone"></i> 05 46 95 60 10</li>
			<li><a href="mailto:contact@larochecourbon.fr"><i class="fas fa-envelope"></i> contact@larochecourbon.fr</a></li>
			<li><a href="http://www.larochecourbon.fr/" target="_BLANK"><i class="fas fa-globe"></i> Accéder au site</a></li>
			<li><a href="https://www.facebook.com/larochecourbon/" target="_BLANK"><i class="fab fa-facebook-square"></i> Accéder à la page Facebook</a></li>
		</ul>		
	</li>

	<li>
		<h3><img src="./public/images/partners/st_jean_d_angle.jpg" alt="Logo Château fort de Saint Jean d'Angle" />Château fort de Saint Jean d'Angle</h3>
		<ul class="infos_partner">
			<li>Classé Monument Historique et gratifié du Grand Prix des Maisons Françaises et du Prix Europa Nostra</li>
			<li><i class="fas fa-phone"></i> 05 46 82 76 03</li>
			<li><a href="mailto:contact@chateausaintjeandangle.fr"><i class="fas fa-envelope"></i> contact@chateausaintjeandangle.fr</a></li>
			<li><a href="http://www.chateausaintjeandangle.fr/" target="_BLANK"><i class="fas fa-globe"></i> Accéder au site</a></li>
			<li><a href="https://www.facebook.com/Chateaufortdesaintjeandangle/" target="_BLANK"><i class="fab fa-facebook-square"></i> Accéder à la page Facebook</a></li>
		</ul>		
	</li>
</ul>

<h2>Artisans, Artistes et cie</h2>

<ul class="liste_partenaires">
	<li>
		<h3><img src="./public/images/partners/christelle_fau.jpg" alt="Logo Christelle Fau - Développeur Web" />Christelle Fau - Développeur Web</h3>
		<ul class="infos_partner">
			<li>Christelle Fau est notre développeur web. Elle s'occupe également de la communication multimédia et print.</li>
			<li><a href="mailto:fau.christelle.17@gmail.com"><i class="fas fa-envelope"></i> fau.christelle.17@gmail.com</a></li>
			<li><a href="https://christelle-fau.fr" target="_BLANK"><i class="fas fa-globe"></i> Accéder au site</a></li>
		</ul>		
	</li>
	<li>
		<h3><img src="./public/images/partners/chas_pitre.png" alt="Logo L'Atelier du Chas Pitre" />L'Atelier du Chas Pitre - Travail du cuir</h3>
		<ul class="infos_partner">
			<li>L'Atelier du Chas Pitre, c'est un atelier nomade de travail artisanal du cuir à tannage végétal, de la corne et du bois.</li>
			<li><a href="mailto:lechaspitre@laposte.net"><i class="fas fa-envelope"></i> lechaspitre@laposte.net</a></li>
			<li><a href="https://www.etsy.com/fr/shop/lAtelierduChasPitre?ref=profile_shopicon" target="_BLANK"><i class="fas fa-globe"></i> Accéder au site</a></li>
			<li><a href="https://www.facebook.com/LAtelier-du-Chas-Pitre-1644675242521585/" target="_BLANK"><i class="fab fa-facebook-square"></i> Accéder à la page Facebook</a></li>
		</ul>		
	</li>

	<li>
		<h3><img src="./public/images/partners/pierre_photo.jpg" alt="Logo Pierre Noirault Photographe" />Pierre Noirault - Photographe</h3>
		<ul class="infos_partner">
			<li>Photographier l'invisible... saisir l'émotion...Concerts, théâtre, impro, spectacles de rue, Urbex, tenter de capter l'instant...</li>
			<li><i class="fas fa-phone"></i> 06 60 59 09 15</li>
			<li><a href="mailto:pierrenoirault@free.fr"><i class="fas fa-envelope"></i> pierrenoirault@free.fr</a></li>
			<li><a href="http://pierrenoiraultphoto.com/" target="_BLANK"><i class="fas fa-globe"></i> Accéder au site</a></li>
			<li><a href="https://www.facebook.com/Pierre-Noirault-Photo-Pro-717636308262267/" target="_BLANK"><i class="fab fa-facebook-square"></i> Accéder à la page Facebook</a></li>
		</ul>		
	</li>

	<li>
		<h3><img src="./public/images/partners/jc_chaudron_photo.jpg" alt="Logo Jean-Christophe Chaudron" />Jean-Christophe Chaudron - Photographe</h3>
		<ul class="infos_partner">
			<li>Photographe à I-Déclic</li>
			<li><a href="https://www.facebook.com/jeanchristophe.chaudron" target="_BLANK"><i class="fab fa-facebook-square"></i> Accéder à la page Facebook</a></li>
		</ul>		
	</li>

	<li>
		<h3><img src="./public/images/partners/i-declic.jpg" alt="Logo I-Déclic" />I-Déclic - Photographe</h3>
		<ul class="infos_partner">
			<li>Parce que dans le monde actuel, l'image devient de plus en plus importante, nous nous sommes réunis pour lui offrir notre meilleur savoir faire !
			Portraits, événements, entreprise, scolaire...Une équipe de professionnels au service de votre image !</li>
			<li><a href="mailto:i-declic@orange.fr"><i class="fas fa-envelope"></i> i-declic@orange.fr</a></li>
			<li><a href="http://vision-full-frame.kabook.fr/" target="_BLANK"><i class="fas fa-globe"></i> Accéder au site</a></li>
			<li><a href="" target="_BLANK"><i class="fab fa-facebook-square"></i> Accéder à la page Facebook</a></li>
		</ul>		
	</li>

	<li>
		<h3><img src="./public/images/partners/mousseline.jpg" alt="Logo Mousseline et Mandragore" /> Mousseline et Mandragore - Couturière</h3>
		<ul class="infos_partner">
			<li>Costumes d'époque, déguisements, histoire, objets anciens, sorties historiques, shootings, mode pour enfants ... les folies d'une passionnée de couture !</li>
			<li><a href="mailto:mousseline.mandragore25@gmail.com"><i class="fas fa-envelope"></i> mousseline.mandragore25@gmail.com</a></li>
			<li><a href="http://mousselinemandrago.wixsite.com/costumes" target="_BLANK"><i class="fas fa-globe"></i> Accéder au site</a></li>
			<li><a href="https://www.facebook.com/mousselineetmandragore/" target="_BLANK"><i class="fab fa-facebook-square"></i> Accéder à la page Facebook</a></li>
		</ul>		
	</li>

	<!-- <li>
		<h3><img src="./public/images/partners/melizenn.png" alt="Logo Melizenn" /> Melizenn - Cosplayeuse</h3>
		<ul class="infos_partner">
			<li>Cosplayeuse depuis 2009, je m’applique à fabriquer tous mes costumes et accessoires par moi même.</li>
			<li><a href="mailto:melizenn@gmail.com"><i class="fas fa-envelope"></i> melizenn@gmail.com</a></li>
			<li><a href="http://www.melizenncosplay.com/" target="_BLANK"><i class="fas fa-globe"></i> Accéder au site</a></li>
			<li><a href="https://www.facebook.com/pg/Melizenncosplay" target="_BLANK"><i class="fab fa-facebook-square"></i> Accéder à la page Facebook</a></li>
		</ul>		
	</li> -->
	
<!--  Geoffrey Forgeron ? Voir avec lui
	<li>
		<h3><img src="./public/images/partners/" alt="Logo " /></h3>
		<ul class="infos_partner">
			<li></li>
			<li><a href="mailto:"><i class="fas fa-envelope"></i> </a></li>
			<li><a href="" target="_BLANK"><i class="fas fa-globe"></i> </a></li>
			<li><a href="" target="_BLANK"><i class="fab fa-facebook-square"></i> </a></li>
		</ul>		
	</li> -->
</ul>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>