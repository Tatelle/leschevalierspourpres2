<?php $title = 'Nos Partenaires - Les Chevaliers Pourpres'; ?>

<?php ob_start(); ?>

<h1>Nos Prestations</h1>

<p class="intro">L'association se compose uniquement de bénévoles. Par conséquent, les ateliers varient en fonction des personnes présentes.<br/>
La troupe s'efforce de faire revivre la vie de camp telle qu'elle pouvait être lorsque les Chevaliers de la table ronde partaient en quête ou lors de rassemblements et de joutes courtoises.</p>

<?php
while ($data = $services->fetch())
{ ?>
	<div class="service">
		<img class="images_services" src="./public/images/services/<?= htmlspecialchars($data['id']) ?>.jpg" alt="Combat de chevaliers" />

		<div>
			<h2><?= htmlspecialchars($data['title']) ?></h2>

			<p><?= nl2br(htmlspecialchars($data['content'])) ?></p>
		</div>
	</div>

	<?php
}
$services->closeCursor();
?>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>