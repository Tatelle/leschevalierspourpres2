<?php
session_start();

require_once('controller/frontend.php');
require_once('controller/backend.php');

try {

    if (isset($_GET['action'])) {

        if ($_GET['action'] == "coming_events") {
            comingEvents();
        }
        elseif ($_GET['action'] == "association") {
            association();
        }
        elseif ($_GET['action'] == "services") {
            services();
        }
        elseif($_GET['action'] == "partners") {
            partners();
        }
        elseif ($_GET['action'] == "contact")
        {
            contact();
        }
        elseif ($_GET['action'] == "send_mail") {
            if (!empty($_POST['nom_contact']) && !empty($_POST['mail_contact']) && !empty($_POST['sujet']) && !empty($_POST['message']) && trim($_POST['nom_contact']) != '' && trim($_POST['mail_contact']) != '' && trim($_POST['sujet']) != '' && trim($_POST['message']) != '' ) {
                sendMail(trim(htmlspecialchars($_POST['nom_contact'])), trim(htmlspecialchars($_POST['mail_contact'])), trim(htmlspecialchars($_POST['sujet'])), trim(htmlspecialchars($_POST['message'])));
            }
            else {
                throw new Exception('Tous les champs ne sont pas remplis !');
            }
        }
        elseif ($_GET['action'] == "signin") {
            signin();
        }
        elseif ($_GET['action'] == "login") {
            login();
        }
        elseif ($_GET['action'] == "admin") {
            adminIndex();
        }
        elseif ($_GET['action'] == "logout") {
            logout();
        }
        elseif ($_GET['action'] == "adminProfil") {
            adminProfil();
        }
        elseif ($_GET['action'] == "adminChangeLogin") {
            if (!empty($_POST['login']) && trim($_POST['login']) != '') {
                adminChangeLogin(trim(htmlspecialchars($_POST['login'])));
            }
            else {
               throw new Exception('Tous les champs ne sont pas remplis !');
            }
        }
        elseif ($_GET['action'] == "adminChangePassword") {
           if (!empty($_POST['pass_old']) && !empty($_POST['pass_new']) && !empty($_POST['pass_confirm']) && trim($_POST['pass_old']) != '' && trim($_POST['pass_new']) != '' && trim($_POST['pass_confirm']) != '') {
                $checkPass = adminCheckOldPassword($_SESSION['login'], trim(htmlspecialchars($_POST['pass_old'])));
                if($checkPass){
                    if($_POST['pass_new'] == $_POST['pass_confirm']){
                        adminChangePassword($_SESSION['id'], trim(htmlspecialchars($_POST['pass_new'])));
                    }else{
                        throw new Exception('Les mots de passe sont différents !');
                    }
                    
                }else{
                    throw new Exception('Mot de passe incorrect ! Nous ne pouvons enregistrer votre nouveau mot de passe.');
                }
                //adminChangeLogin(trim(htmlspecialchars($_POST['login'])));
            }
            else {
               throw new Exception('Tous les champs ne sont pas remplis !');
            }
        }
        elseif ($_GET['action'] == "adminNewUser") {
            adminNewUser();
        }
        elseif ($_GET['action'] == "adminAddUser") {
            if (!empty($_POST['login']) && !empty($_POST['pass_new']) && !empty($_POST['pass_confirm']) && trim($_POST['login']) != '' && trim($_POST['pass_new']) != '' && trim($_POST['pass_confirm']) != '') {
                if ($_POST['pass_new'] == $_POST['pass_confirm']) {
                    adminAddUser(trim(htmlspecialchars($_POST['login'])), trim(htmlspecialchars($_POST['pass_new'])));
                }else{
                    throw new Exception('Les mots de passes sont différents !');
                }
            }
            else {
                throw new Exception('Tous les champs ne sont pas remplis !');
            }
        }
        elseif ($_GET['action'] == "adminNewEvent") {
            adminNewEvent();
        }
        elseif ($_GET['action'] == "adminAddEvent") {
            if (!empty($_POST['title']) && !empty($_POST['eventContent']) && !empty($_POST['place']) && !empty($_POST['start_date']) && !empty($_POST['end_date']) && trim($_POST['title']) != '' && trim($_POST['eventContent']) != '' && trim($_POST['place']) != '' && $_POST['start_date'] != '' && $_POST['end_date'] != '') {

                $testStartDate = testDate($_POST['start_date']);
                $testEndDate = testDate($_POST['end_date']);

                if ($testStartDate == true && $testEndDate == true){

                    $date1 = new DateTime($_POST['start_date']);
                    $date2 = new DateTime($_POST['end_date']);

                    if( $date1 < $date2 ){
                        adminAddEvent(trim(htmlspecialchars($_POST['title'])), trim(htmlspecialchars($_POST['eventContent'])), trim(htmlspecialchars($_POST['place'])), htmlspecialchars($_POST['start_date']), htmlspecialchars($_POST['end_date']));
                    }
                    else{
                        throw new Exception("Attention ! La date de fin d'évenement que vous avez sélectionné est antérieure à la date de début !");
                    }
                }
                else{
                    throw new Exception("Le format des dates n'est pas conforme.");
                }
            }
            else {
                throw new Exception('Tous les champs ne sont pas remplis !');
            }
        }
        elseif ($_GET['action'] == "adminAllEvents") {
            adminAllEvents();
        }
        // Modification d'un évenement
        elseif ($_GET['action'] == "adminChangeEvent") {

            if (isset($_GET['id']) && $_GET['id'] > 0) {
                $exist = checkEvent($_GET['id']);

                if($exist['event_exist']){
                    adminChangeEvent($_GET['id']);
                }
                else {
                    throw new Exception('Oups ! L\'évenement que vous souhaitez modifier n\'existe pas (ou plus).');
                }
            }
            else {
                throw new Exception('Aucun identifiant de billet envoyé');
            }
        }
        elseif ($_GET['action'] == 'adminChangingEvent') {
            if (isset($_GET['id']) && $_GET['id'] > 0) {
                $exist = checkEvent($_GET['id']);

                if ($exist['event_exist']) {
                    adminChangingEvent($_GET['id'], trim(htmlspecialchars($_POST['title'])), trim(htmlspecialchars($_POST['eventContent'])), trim(htmlspecialchars($_POST['place'])), htmlspecialchars($_POST['start_date']), htmlspecialchars($_POST['end_date']));
                }
                else{
                    throw new Exception('Oups ! Impossible de mettre à jour l\'évenement car il n\'existe pas (ou plus).');
                }

            }
            else {
                throw new Exception('Aucun identifiant de chapitre envoyé');
            }
        }
        elseif ($_GET['action'] == "adminDeleteEvent") {
            
            if (isset($_GET['id']) && $_GET['id'] > 0) {
                $eventExist = checkEvent($_GET['id']);
                if($eventExist['event_exist']){
                    adminDeleteEvent($_GET['id']);
                }
                else{
                    throw new Exception('L\'évenement que vous souhaitez supprimer n\'existe pas (ou plus) !');
                }
            }
            else {
                throw new Exception('Aucun identifiant de chapitre et/ou de commentaire envoyé');
            }
        }



        elseif ($_GET['action'] == "adminNewService") {
            adminNewService();
        }
        elseif ($_GET['action'] == "adminAddService") {
            if (!empty($_POST['title']) && !empty($_POST['serviceContent']) && !empty($_POST['serviceImage']) && trim(htmlspecialchars($_POST['title'])) != '' && trim(htmlspecialchars($_POST['serviceContent'])) != '') {
                if ($_FILES['serviceImage']['error'] > 0){
                    throw new Exception('Erreur lors du transfert de la photo');
                }
                else{
                    if ($_FILES['icone']['size'] > $maxsize){
                        throw new Exception('Le fichier est trop gros');
                    }
                    else{
                        echo "coucou";
                        //adminAddService(trim(htmlspecialchars($_POST['title'])), trim(htmlspecialchars($_POST['serviceContent'])));
                    } 
                } 
                    
            }
            else {
                throw new Exception('Tous les champs ne sont pas remplis !');
            }
        }
        // elseif ($_GET['action'] == "adminAllServices") {
        //     adminAllEvents();
        // }
        // elseif ($_GET['action'] == "adminChangeService") {

        //     if (isset($_GET['id']) && $_GET['id'] > 0) {
        //         $exist = checkEvent($_GET['id']);

        //         if($exist['event_exist']){
        //             adminChangeEvent($_GET['id']);
        //         }
        //         else {
        //             throw new Exception('Oups ! L\'évenement que vous souhaitez modifier n\'existe pas (ou plus).');
        //         }
        //     }
        //     else {
        //         throw new Exception('Aucun identifiant de billet envoyé');
        //     }
        // }
        // elseif ($_GET['action'] == 'adminChangingService') {
        //     if (isset($_GET['id']) && $_GET['id'] > 0) {
        //         $exist = checkEvent($_GET['id']);

        //         if ($exist['event_exist']) {
        //             adminChangingEvent($_GET['id'], htmlspecialchars($_POST['title']), htmlspecialchars($_POST['eventContent']), htmlspecialchars($_POST['place']), htmlspecialchars($_POST['start_date']), htmlspecialchars($_POST['end_date']));
        //         }
        //         else{
        //             throw new Exception('Oups ! Impossible de mettre à jour l\'évenement car il n\'existe pas (ou plus).');
        //         }

        //     }
        //     else {
        //         throw new Exception('Aucun identifiant de chapitre envoyé');
        //     }
        // }
        // elseif ($_GET['action'] == "adminDeleteService") {
            
        //     if (isset($_GET['id']) && $_GET['id'] > 0) {
        //         $eventExist = checkEvent($_GET['id']);
        //         if($eventExist['event_exist']){
        //             adminDeleteEvent($_GET['id']);
        //         }
        //         else{
        //             throw new Exception('L\'évenement que vous souhaitez supprimer n\'existe pas (ou plus) !');
        //         }
        //     }
        //     else {
        //         throw new Exception('Aucun identifiant de chapitre et/ou de commentaire envoyé');
        //     }
        //     //adminDeleteEvent();
        // }



        else {
            throw new Exception('Oups ! La page que vous cherchez n\'existe pas (ou plus).');
        }
    }
    else {
        home();
    }
}
catch(Exception $e) {
    error($e);
}
