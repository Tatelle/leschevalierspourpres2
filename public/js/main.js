var regMail = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');
var regPass = new RegExp('[A-Z][a-z][0-9]','i');

// check if the provided jQuery element has a value
var isFormElementValid = function(formElement, optionalFunction) {
    return formElement.val().trim().length > 0 && ( optionalFunction === undefined || optionalFunction(formElement) ) ;
}
 
var checkMail = function(element) {
    if(regMail.test(element.val()) === true ) {
        $("#error_mail").addClass("hidden");
        element.removeClass("has-error");
        return true;
    } else {
        $("#error_mail").removeClass("hidden");
        element.addClass("has-error");
        return false;
    }
}

var checkPassword = function(element) {
    if(regPass.test(element.val()) === true && element.val().length > 7) {
        console.log('mots de passe correct');
        $("#error_true_pass").addClass("hidden");
         if($(this).attr("id") === "pass_new" && $("#pass_confirm").val().trim() === "" ){
            return false; 
        }

        if( $("#pass_new").val() === $("#pass_confirm").val() ){
            console.log('mots de passe identiques');
            $("#error_password").addClass("hidden");
            return true;
        }
        else{
            console.log('mots de passe différents');
            $("#error_password").removeClass("hidden");
            return false;
        }
    }
    else{
        console.log('mots de passe non conforme');
        $("#error_true_pass").removeClass("hidden");
        return false;
    }
}

var verification = function(formArray, validated, invalidated) {
    var areFormsOk = true;

    formArray.forEach(function(customObject) {
        areFormsOk = areFormsOk && isFormElementValid(customObject.element, customObject.validationFunction);
    });
 
    if (areFormsOk === true)  {
        validated();
    } else {
        invalidated();         
    }
}
 
var inputsJQuery = [
    { element: $("#nom_contact") },
    { element: $("#sujet") },
    { element: $("#message") },
    { element: $("#mail_contact"), validationFunction: checkMail}
];

var inputsJQuery2 = [
    { element: $("#login") }
];

var inputsJQuery3 = [
    { element: $("#pass_old") },
    { element: $("#pass_new") },
    { element: $("#pass_confirm"), validationFunction: checkPassword}
];
 
var formValid = function() {
    $("#bouton_envoyer").removeClass("disabled");
    $("#bouton_envoyer").removeAttr("disabled");
}
 
var formInvalid = function() {
    $("#bouton_envoyer").addClass("disabled");
    $("#bouton_envoyer").attr("disabled", "disabled");
}

var formValid2 = function() {
    $("#bouton_sauvegarder").removeClass("disabled");
    $("#bouton_sauvegarder").removeAttr("disabled");
}
 
var formInvalid2 = function() {
    $("#bouton_sauvegarder").addClass("disabled");
    $("#bouton_sauvegarder").attr("disabled", "disabled");
}
 
inputsJQuery.forEach(function(customObject) {
  customObject.element.on("keyup", function() {
      verification(inputsJQuery, formValid, formInvalid)
  });
});

inputsJQuery2.forEach(function(customObject) {
  customObject.element.on("keyup", function() {
      verification(inputsJQuery2, formValid2, formInvalid2)
  });
});

inputsJQuery3.forEach(function(customObject) {
  customObject.element.on("keyup", function() {
      verification(inputsJQuery3, formValid, formInvalid)
  });
});
