<?php $title = 'Admin - Ajouter une prestation'; ?>

<?php ob_start(); ?>

<h1>Edition d'une nouvelle prestation</h1>

<p class="bouton_retour"><a href="index.php?action=admin"><button class="btn btn-default">Retour à l'accueil</button></a></p>

<form action="index.php?action=adminAddService" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <label for="title">Titre de la prestation :</label>
        <input type="text" id="title" name="title"  class="form-control input-sm" required autofocus/>
    </div>
    <div class="form-group">
        <label for="serviceContent">Description de la prestation :</label>
        <textarea  id="serviceContent" name="serviceContent" class="form-control" required></textarea>
    </div>
    <div class="form-group">
        <label for="serviceImage">Image de la prestation :</label>
        <input type="hidden" name="MAX_FILE_SIZE" value="150000" />
        <input type="file" id="title" class="form-control" name="serviceImage" required />
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-success" value="Enregistrer la prestation" />
    </div>
</form>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>