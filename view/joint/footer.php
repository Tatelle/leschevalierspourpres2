 <footer class="container">
        	<div id="liens_footer">
        		<p><a href="#" >Mentions légales</a></p>
        		<p>
        			<a href="https://www.facebook.com/Chevaliers-Pourpres-184199298278086/" target="_BLANK">Aimer notre page <i class="fab fa-facebook-square"></i></a><br/>
        			<a href="http://chevalierspourpres.forum-actif.net" target="_BLANK">Accéder au Forum <i class="fas fa-chess-rook"></i></a>
        		</p>
        		
				<?php
				if (!isset($_SESSION['login'])) //On est dans la page de formulaire
				{ ?>
		          	<p><a href="./index.php?action=signin">Connexion</a></p>
		        <?php
				}
				else { 

					if(isset($_GET['action']) && ($_GET['action'] == 'admin' || $_GET['action'] == 'adminNewEvent' || $_GET['action'] == 'adminAllEvents' || $_GET['action'] == 'adminChangeEvent')) { ?>
						<p><a href="index.php">Accéder au Site</a><br/>
					<?php } 
					else { ?>
						<p><a href="index.php?action=admin">Accéder à la partie Admin</a><br/>
					<?php } ?>
					<a href="index.php?action=logout">Déconnexion</a></p>
				<?php } ?>
        	</div>
        	<p id="copyright">Tous droits réservés © Christelle Fau 2018 - Formation Openclassrooms - Développeur Web Junior - Projet n°5 : Présentez librement un projet personnel</p>
        </footer>

	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	    <script>window.jQuery || document.write('<script src="vendor/bootstrap/docs/assets/js/vendor/jquery.min.js"><\/script>')</script>
	    <script src="vendor/bootstrap/docs/dist/js/bootstrap.min.js"></script>
	    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	    <script src="vendor/bootstrap/docs/assets/js/ie10-viewport-bug-workaround.js"></script>

	    <script src="public/js/main.js"></script>

    </body>
</html>