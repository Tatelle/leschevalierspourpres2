<?php $title = 'L\'Association - Les Chevaliers Pourpres'; ?>

<?php ob_start(); ?>

<h1>L'Association</h1>

<section class="historique">
	<h2>Historique et politique de l'Association</h2>
	<div class="intro-asso">
		<div  class="image-asso"><img src="./public/images/defile.jpg" alt="défilé des Chevaliers Pourpres - Fête multi-époque de Brouage 2017" /></div>
		<div class="description-asso">
			<p>Créée en 2002 par Benjamin Vincent, notre association s'attache à <span class="strong">l'évocation médiévale de la légende arthurienne</span>. Il s'agit pour nous d'évoquer la <span class="strong">vie des chevaliers de la Table Ronde</span>, ce à quoi pourrait ressembler leur univers. Malgré ce cadre issu du merveilleux, notre association s'attache à une certaine <span class="strong">crédibilité historique</span>. Si nos armures et équipements sont parfois très différents les uns des autres, de par les époques représentées et leur lieu de provenance, chaque membre de notre association attache une grande importance à la constitution d'un équipement civil et militaire cohérent.</p>
			<p>Nos activités sont très variées, la prestation phare de l’association se porte sur des <span class="strong">combats médiévaux</span>, qu’ils soient chorégraphiés ou non lors de tournois (avec des armes mouchetées pour éviter les accidents). L’association dispose aussi d’un grand nombre d’ateliers : démonstration de fabrication de cotte de maille, calligraphie, jeux historiques et merveilleux, contes, frappe de monnaie, fabrication de bougies, filage... et de forge. L’association dispose aussi d’un camp militaire médiéval</p>
			<p>Nous possédons bien sûr tout le nécessaire de campement médiéval : tentes, auvents, cuisine, etc...</p>
		</div>
	</div>
</section>

<section class="membres-ca">
	<h2>Les membres du Bureau</h2>

	<div class="bureau">
		<div class="membre">
			<p><img src="./public/images/bureau/joanna_roux-mahieux.jpg" alt="Joanna Roux-Mahieux : Président" /></p>
			<h3>Joanna Roux-Mahieux</h3>
			<p>Présidente</p>
		</div>
		
		<div class="membre">
			<p><img src="./public/images/bureau/didier_salmon.jpg" alt="Didier Salmon : Trésorier" /></p>
			<h3>Didier Salmon</h3>
			<p>Trésorier</p>
		</div>
		
		<div class="membre">
			<p><img src="./public/images/bureau/geoffrey_maiano.jpg" alt="Geoffrey Maïano : Secrétaire" /></p>
			<h3>Geoffrey Maïano</h3>
			<p>Secrétaire</p>
		</div>

	</div>

	<h2>Les membres du CA</h2>

	<div class="ca">	
		<div class="membre">
			<p><img src="./public/images/bureau/christelle_fau.jpg" alt="Christelle Fau : Membre du CA - Webmaster" /></p>
			<h3>Christelle Fau</h3>
			<!-- <p>Webmaster - Responsable de la communication multimédia</p> -->
		</div>

		<div class="membre">
			<p><img src="./public/images/bureau/julie_angibaud.jpg" alt="Julie Angibaud : Membre du CA" /></p>
			<h3>Julie Angibaud</h3>
			<!-- <p></p> -->
		</div>

		<div class="membre">
			<p><img src="./public/images/bureau/antoine_artus.jpg" alt="Antoine Artus : Membre du CA" /></p>
			<h3>Antoine Artus</h3>
			<!-- <p></p> -->
		</div>

		<div class="membre">
			<p><img src="./public/images/bureau/kim_chauvet.jpg" alt="Kim Chauvet : Membre du CA" /></p>
			<h3>Kim Chauvet</h3>
			<!-- <p></p> -->
		</div>

		
		<div class="membre">
			<p><img src="./public/images/bureau/antoine_jarriault2.jpg" alt="Antoine Jarriault : Membre du CA" /></p>
			<h3>Antoine Jarriault</h3>
			<!-- <p></p> -->
		</div>
	</div>
</section>

<?php 
	$content = ob_get_clean();
	require('template.php'); 
?>