<?php

namespace LesChevaliersPourpes\Site\Model;

require_once("model/Manager.php");
require_once 'model/Configuration.php';

class ComingEventsManager extends Manager
{
    // Cette fonction permet de récupérer les évenements à venir
    public function getComingEvents()
    {
        
        
        $sql = 'SELECT id, title, content, place, DATE_FORMAT(start_date, \'%d/%m/%Y\') AS start_date_fr, DATE_FORMAT(end_date, \'%d/%m/%Y\') AS end_date_fr FROM cp_coming_events WHERE end_date > NOW() ORDER BY start_date';
        $req = $this->executerRequete($sql);

        return $req;

        // $db = $this->dbConnect();
        // $req = $db->query('SELECT id, title, content, place, DATE_FORMAT(start_date, \'%d/%m/%Y\') AS start_date_fr, DATE_FORMAT(end_date, \'%d/%m/%Y\') AS end_date_fr FROM cp_coming_events WHERE end_date > NOW() ORDER BY start_date');

        // return $req;

        
    }
    // Cette fonction permet de récupérer un évènement en particulier
    public function getComingEvent($eventId)
    {  
        $sql = 'SELECT * FROM cp_coming_events WHERE id = ?';
        $req = $this->executerRequete($sql, array($eventId));
        $event = $req->fetch();

        return $event;
        
  //       $db = $this->dbConnect();
  //       $req = $db->prepare('SELECT * FROM cp_coming_events WHERE id = ?');
  //       // $req = $db->prepare('SELECT id, title, content, place, DATE_FORMAT(start_date, \'%d/%m/%Y\') AS start_date_fr, DATE_FORMAT(end_date, \'%d/%m/%Y\') AS end_date_fr FROM cp_coming_events WHERE id = ?');
		// $req->execute(array($eventId));
        // $event = $req->fetch();

        // return $event;


    }
}