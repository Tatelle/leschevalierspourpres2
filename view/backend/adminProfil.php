<?php $title = 'Admin - Profil'; ?>

<?php ob_start(); 

if(isset($_SESSION['message_profil'])){ ?>
	<p class="alert alert-success" id="message_erreur"><strong><?= $_SESSION['message_profil'] ?></strong></p>
	<?php
	unset($_SESSION['message_profil']);
}
if(isset($_SESSION['message_erreur'])){ ?>
	<p class="alert alert-danger" id="message_erreur"><strong><?= $_SESSION['message_erreur'] ?></strong></p>
	<?php
	unset($_SESSION['message_erreur']);
} ?>

<h1>Configuration du profil</h1>

<p class="bouton_retour"><a href="index.php?action=admin"><button class="btn btn-default">Retour à l'accueil</button></a></p>

<h2>Changer d'Identifiant</h2>
<form action="index.php?action=adminChangeLogin" method="POST">
    <div class="form-group">
        <label for="login">Identifiant :</label>
        <input type="text" id="login" name="login" placeholder="Nouvel identifiant" value="<?= $_SESSION['login'] ?>" class="form-control input-sm" autofocus />
    </div>
    
    <div class="form-group">
        <input id="bouton_sauvegarder" type="submit" class="btn btn-success" value="Valider" disabled="disabled" />
    </div>
</form>

<h2>Changer de mot de passe</h2>
<form action="index.php?action=adminChangePassword" method="POST">

    
    <div class="form-group">
        <label for="pass_old">Ancien mot de passe :</label>
        <input type="password" name="pass_old" id="pass_old" placeholder="Mot de passe actuel"  class="form-control input-sm" autofocus/>
    </div>
    <div class="form-group">
        <label for="pass_new">Nouveau mot de passe : (Le mot de passe doit contenir au moins un chiffre, une majuscule, une minuscule et doit avoir une longueur minimale de 8 caractères)</label>
        <input type="password" name="pass_new" id="pass_new" placeholder="Nouveau mot de passe" class="form-control input-sm"/>
    </div>
    <div class="form-group">
        <label for="pass_confirm">Confirmer nouveau mot de passe : </label>
        <input type="password" name="pass_confirm" id="pass_confirm" placeholder="Confirmer mot de passe" class="form-control input-sm"/>
        <label id="error_password" class="hidden pull-left red">Mots de passe différents !</label><br/>
        <label id="error_true_pass" class="hidden pull-left red">Le mot de passe n'est pas conforme aux conditions de validation !</label><br/>
   	</div>
    <div class="form-group">
        <input  id="bouton_envoyer" type="submit" class="btn btn-success" value="Valider" disabled="disabled" />
    </div>
</form>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>