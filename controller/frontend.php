<?php

use \LesChevaliersPourpes\Site\Model\AdminManager;
use \LesChevaliersPourpes\Site\Model\Manager;
use \LesChevaliersPourpes\Site\Model\ComingEventsManager;
use \LesChevaliersPourpes\Site\Model\ContactManager;

require_once('model/AdminManager.php');
require_once('model/Manager.php');
require_once('model/ComingEventsManager.php');
require_once('model/ContactManager.php');

function home()
{
	$comingEventsManager = new ComingEventsManager();
    $comingEvents = $comingEventsManager->getComingEvents();
    
    require('view/frontend/home.php');
}

function association()
{
	require('view/frontend/association.php');
}

function comingEvents()
{
    $comingEventsManager = new ComingEventsManager();
    $comingEvents = $comingEventsManager->getComingEvents();
    
    require('view/frontend/comingEvents.php');
}

function services()
{
    $servicesManager = new AdminManager();
    $services = $servicesManager->getServices();
    
    require('view/frontend/services.php');
}

function partners()
{
    require('view/frontend/partners.php');
}

function contact()
{
    require('view/frontend/contact.php');
}

function sendMail($contactName, $contactMail, $subject, $message)
{
    $sendMailManager = new ContactManager();
    $sendingMail = $sendMailManager->getSendMail($contactName, $contactMail, $subject, $message);
    
	require('view/frontend/sendMail.php');
	
	//header('Location:http://www.les-chevaliers-pourpres.fr/');
	
}

function signin()
{
    require('view/frontend/signin.php');
}

function error($e){
	require('view/frontend/errorView.php');    
}