<?php $title = 'Événements à venir - Les Chevaliers Pourpres'; ?>

<?php ob_start(); ?>

<h1>Événements à venir</h1>

<?php
while ($data = $comingEvents->fetch())
{ ?>
	<div class="event">
		<div class="header_event">
			<h2><?= htmlspecialchars($data['title']) ?></h2>
			<p class="lieu"><?= htmlspecialchars($data['place']) ?></p>
			<?php

            if( htmlspecialchars($data['start_date_fr']) == htmlspecialchars($data['end_date_fr']) ) { ?>
            	<p class="date">Le <?= htmlspecialchars($data['start_date_fr']) ?></p>
            <?php 
            }
            else{
            	?>
				<p class="date">Du <?= htmlspecialchars($data['start_date_fr']) ?> au <?= htmlspecialchars($data['end_date_fr']) ?></p>
			<?php } ?>

		</div>
		<p class="description"><?= nl2br(htmlspecialchars($data['content'])) ?></p>
	</div>
	<?php
}
$comingEvents->closeCursor();
?>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>