<?php

namespace LesChevaliersPourpes\Site\Model;

require_once("model/Manager.php");

class ContactManager extends Manager
{
    public function getSendMail($contactName, $contactMail, $subject, $message)
    {
        $sql = 'INSERT INTO `cp_sent_emails`(`name`, `contact_mail`, `subject`, `message`, `sending_date`) VALUES(?, ?, ?, ?, NOW())';
        $req = $this->executerRequete($sql);

		return $req;


        // $db = $this->dbConnect();
        // $req = $db->prepare('INSERT INTO `cp_sent_emails`(`name`, `contact_mail`, `subject`, `message`, `sending_date`) VALUES(?, ?, ?, ?, NOW())');
        
        // $affectedLines = $req->execute(array($contactName, $contactMail, $subject, $message));

        // return $affectedLines;
    }
}