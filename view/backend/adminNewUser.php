<?php $title = 'Ajouter un Utilisateur - Admin'; ?>

<?php ob_start();

if(isset($_SESSION['message_profil'])){ ?>
	<p class="alert alert-success" id="message_erreur"><strong><?= $_SESSION['message_profil'] ?></strong></p>
	<?php
	unset($_SESSION['message_profil']);
}
if(isset($_SESSION['message_erreur'])){ ?>
	<p class="alert alert-danger" id="message_erreur"><strong><?= $_SESSION['message_erreur'] ?></strong></p>
	<?php
	unset($_SESSION['message_erreur']);
} ?>

<h1>Création d'un nouvel utilisateur</h1>

<p class="bouton_retour"><a href="index.php?action=admin"><button class="btn btn-default">Retour à l'accueil</button></a></p>

<form action="index.php?action=adminAddUser" method="POST">
    <div class="form-group">
        <label for="login">Identifiant :</label>
        <input type="text" id="login" name="login" placeholder="Identifiant" <?= (isset($_GET['login']) ? 'value="'.htmlspecialchars($_GET['login']).'"' : ''); ?> class="form-control input-sm" autofocus />
    </div>
    <div class="form-group">
        <label for="pass_new">Mot de passe : (Le mot de passe doit contenir au moins un chiffre, une majuscule, une minuscule et doit avoir une longueur minimale de 8 caractères)</label>
        <input type="password" name="pass_new" id="pass_new" placeholder="Nouveau mot de passe" class="form-control input-sm"/>
    </div>
    <div class="form-group">
        <label for="pass_confirm">Confirmer mot de passe : </label>
        <input type="password" name="pass_confirm" id="pass_confirm" placeholder="Confirmer mot de passe" class="form-control input-sm"/>
        <label id="error_password" class="hidden pull-left red">Mots de passe différents !</label><br/>
        <label id="error_true_pass" class="hidden pull-left red">Le mot de passe n'est pas conforme aux conditions de validation !</label><br/>
   	</div>
    
    <div class="form-group">
        <input id="bouton_sauvegarder" type="submit" class="btn btn-success" value="Valider" disabled="disabled" />
    </div>
</form>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>