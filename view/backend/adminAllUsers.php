<?php $title = 'Liste des utilisateurs - Admin'; ?>

<?php ob_start(); ?>

<h1>Liste des utilisateurs</h1>

<p class="bouton_retour"><a href="index.php?action=admin"><button class="btn btn-default">Retour à l'accueil</button></a></p>

<?php
if(isset($_SESSION['message_event'])){ ?>
	<p class="alert alert-success" id="message_erreur"><strong><?= $_SESSION['message_event'] ?></strong></p>
	<?php
	unset($_SESSION['message_event']);
} 

while ($data = $usersList->fetch())
{ ?>
	<div class="event">
		<div class="header_event">
			<h2><?= htmlspecialchars($data['title']) ?></h2>
		</div>
		<p class="description"><?= nl2br(htmlspecialchars($data['content'])) ?></p>
	</div>
	<?php
}
$usersList->closeCursor();
?>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>