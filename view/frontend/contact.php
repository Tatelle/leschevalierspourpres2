<?php $title = 'Contact - Les Chevaliers Pourpres'; ?>

<?php ob_start(); ?>

<h1>Contactez nous</h1>
<p class="intro">Vous avez un projet à nous soumettre ou souhaitez en savoir plus sur nos prestations ?
	<br/>Contactez-nous !
</p>
<?php 
if (isset($_SESSION['envoimail']))
{ ?>
	<div class="container">
		<p class="alert alert-success" id="message_erreur"><strong><?= $_SESSION['envoimail'] ?></strong></p>
	</div>
	<?php
}
?>

<div id="page_contact">
	<div class="modal-content">
		<form method="post" action="./index.php?action=send_mail" class="col-sm-12">
			<!-- header -->
			<div class="modal-header">
				<h3>Envoyez-nous un mail</h3>

			</div>
			<!-- body -->
			<div class="modal-body">
				<p><label id="error_mail" class="hidden pull-left red">Adresse électronique incorrecte !</label><br/></p>
				<p>
					<label for="nom_contact">Nom Prénom <span class="red">*</span> :</label>
					<br/><input type="text" name="nom_contact" id="nom_contact" />
				</p>

				<p>
					<label for="mail_contact">Adresse Mail <span class="red">*</span> :</label>
					<br/><input type="text" name="mail_contact" id="mail_contact" />
				</p>
				
				<p>
					<label for="sujet">Sujet <span class="red">*</span> :</label>
					<br/><input type="text" name="sujet" id="sujet" />
				</p>
				
				<p>
					<label for="message">Message <span class="red">*</span> :</label>
					<br/><textarea name="message" id="message" rows="10" cols="45"></textarea>
				</p>
				
				<p>
					<input type="checkbox" name="copie" id="copie" /> <label for="copie">M'envoyer une copie</label>
				</p>

				<p><span class="red">*</span> Champs obligatoires</p>

			</div>

			<!-- footer -->
			<div class="modal-footer">
				<input id="bouton_envoyer" class="btn btn-lg btn-primary" disabled="disabled" type="submit" value="Envoyer" />
				<!-- <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button> -->
			</div>
		</form>
	</div>
	<div id="infos_cp">
		<h3>Les Chevaliers Poupres</h3>
		<p>25 Rue du Gros Sillon,
			<br/>17290 Chambon</p>

		<ul class="infos_contact">
			<li><a href="mailto:contact.leschevalierspourpres@gmail.com" title="contact.leschevalierspourpres@gmail.com"><i class="fas fa-envelope"></i> Contact par mail</a></li>
			<li><a href="https://www.facebook.com/Chevaliers-Pourpres-184199298278086/" target="_BLANK"><i class="fab fa-facebook-square"></i> Page Facebook : Chevaliers Pourpres</a></li>
			<li><a href="http://chevalierspourpres.forum-actif.net" target="_BLANK"><i class="fas fa-chess-rook"></i> Accéder au Forum</a></li>
		</ul>
	</div>
</div>

<?php unset($_SESSION['envoimail']);
$content = ob_get_clean(); ?>

<?php require('template.php'); ?>