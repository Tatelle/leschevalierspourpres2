<?php 

$contactMail = strtolower($contactMail);
$today = date("d-m-Y H:i:s");  

if (filter_var($contactMail, FILTER_VALIDATE_EMAIL)) {

	// $destinataire = "parade.christelle@gmail.com";
	$destinataire = "contact.leschevalierspourpres@gmail.com";
	$objet = "Les Chevaliers Pourpres - Nouvelle prise de contact";
	$mail="
	<html>
		<head>
			<meta content='text/html; charset=utf-8' http-equiv='Content-Type'>
			<title>Contact Les Chevaliers Pourpres - Message</title>
		</head>
		<body>
			<h1>Mail de contact Les Chevaliers Pourpres</h1>
			<p>Bonjour,<br/><br/>
			Vous venez de recevoir un mail de la plateforme les-chevaliers-pourpres.fr :</p>
			<p><b>Contact :</b>	<br/>" . $contactName . "<br/>" . $contactMail . "
			<br/>Sujet : " . $subject . "</b></p>
			<p><b>Date demande : </b>" . $today . "</p>
			<p><b>Message : </b>" . $message ."</p>
		</body>
	</html>";
	$headers = "From: Contact - les-chevaliers-pourpres.fr <" . $destinataire .">\r\n";
	$headers .= "X-Sender:" . $destinataire ."\r\n";
	$headers .= "X-Mailer: PHP/".phpversion()."\r\n";
	$headers .= "X-Priority: 2\r\n";
	$headers .= "Return-Path: " . $destinataire ."\r\n";
	$headers .= 'MIME-Version: 1.0' . "\r\n"; 
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	$headers .= "Content-Transfer-Encoding: 7bit\r\n";
	$headers .= "Bcc: fau.christelle.17@gmail.com" . "\r\n";

	$_SESSION['envoimail'] = "Votre mail a été envoyé avec succès ! Nous nous efforçons de vous répondre au plus vite.";

	if(mail($destinataire, $objet, $mail, $headers) ){

		if(isset($_POST['copie'])) {
			$destinataire_copie = $contactMail;
			$objet_copie = "Copie de votre message";
			$message_copie = $message;

			$mail_copie="
			<html>
				<head>
					<meta content='text/html; charset=utf-8' http-equiv='Content-Type'>
					
					<title>Contact Les Chevaliers Pourpres - Message</title>
				</head>
				<body>
					<h1>Envoi de la copie de votre message</h1>
					<p>Bonjour,<br/><br/>
					Vous venez d'envoyer un mail via la plateforme les-chevaliers-pourpres.fr. Nous nous efforçons de répondre à votre demande au plus vite.
					<br/> Voici, ci-dessous votre message :</p>
					<p><b>Contact :</b>	<br/>" .$contactName. "<br/>" . $contactMail . "
					<br/>Sujet : " . $subject . "</b></p>
					<p><b>Date demande : </b>" . $today . "</p>
					<p><b>Message : <br/></b>" . $message_copie ."</p>
				</body>
			</html>";

			$headers_copie = "From: Contact - les-chevaliers-pourpres.fr <" . $destinataire .">\r\n";
			$headers_copie .= "X-Sender:" . $destinataire_copie ."\r\n";
			$headers_copie .= "X-Mailer: PHP/" . phpversion() . "\r\n";
			$headers_copie .= "X-Priority: 2\r\n";
			$headers_copie .= "Return-Path: " . $destinataire ."\r\n";
			$headers_copie .= 'MIME-Version: 1.0' . "\r\n"; 
			$headers_copie .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
			$headers_copie .= "Content-Transfer-Encoding: 7bit\r\n";

			if(!mail($destinataire_copie, $objet_copie, $mail_copie, $headers_copie)) {
				throw new Exception('Impossible de vous envoyer une copie du mail ! Veuillez vérifier l\'adresse mail enregistrée : ' . $contactMail);
			}
		}

	}else{
		throw new Exception('Pour des raisons techniques, il est impossible d\'envoyer votre message pour le moment. Veuillez ressayer ulterieurement ou nous contacter directement à l\'adresse suivante : contact.leschevalierspourpres@gmail.com 
			<br/>N\'hésitez pas à nous signaler l\'anomalie que nous puissions la résoudre au plus vite
			<br/>Nous nous excusons de la gène occasionnée...');
	}
	
}else{
	throw new Exception('Une erreur est survenue lors de l\'envoi du mail ! Votre adresse mail n\'est pas conforme.');
}
header('Location:./index.php?action=contact');