<?php $title = 'Événements à venir - Admin'; ?>

<?php ob_start(); ?>

<h1>Événements à venir</h1>

<p class="bouton_retour"><a href="index.php?action=admin"><button class="btn btn-default">Retour à l'accueil</button></a></p>

<?php
if(isset($_SESSION['message_event'])){ ?>
	<p class="alert alert-success" id="message_erreur"><strong><?= $_SESSION['message_event'] ?></strong></p>
	<?php
	unset($_SESSION['message_event']);
} 

while ($data = $comingEvents->fetch())
{ ?>
	<div class="event">
		<div class="header_event">
			<h2><?= htmlspecialchars($data['title']) ?></h2>
			<p class="lieu"><?= htmlspecialchars($data['place']) ?></p>
			<?php if( htmlspecialchars($data['start_date_fr']) == htmlspecialchars($data['end_date_fr']) ) { ?>
            	<p class="date">Le <?= htmlspecialchars($data['start_date_fr']) ?></p>
            <?php 
            }
            else{
        		?>
				<p class="date">Du <?= htmlspecialchars($data['start_date_fr']) ?> au <?= htmlspecialchars($data['end_date_fr']) ?></p>
			<?php } ?>
		</div>
		<p class="description"><?= nl2br(htmlspecialchars($data['content'])) ?></p>
		<p>
			<a href="index.php?action=adminChangeEvent&amp;id=<?= $data['id'] ?>" data-toggle="tooltip" title="Modifier">
  				<button class="btn btn-warning btn-lg"><span class="glyphicon glyphicon-pencil"></span></button>
  			</a> 
  			<a href="index.php?action=adminDeleteEvent&amp;id=<?= $data['id'] ?>" data-toggle="tooltip" title="Supprimer" onclick="return(confirm('Etes-vous sûr de vouloir supprimer cet évenement ?'));">
	          <button class="btn btn-danger btn-lg"><span class="glyphicon glyphicon-remove"></span></button>
	        </a>
	    </p>
	</div>
	<?php
}
$comingEvents->closeCursor();
?>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>