<?php $title = 'Connexion Admin'; ?>

<?php ob_start(); 

if (!isset($_SESSION['login'])) {

  if(isset($_SESSION['message'])) { ?>
    <div class="form-group"><p class="alert alert-danger" id="message_erreur"><strong><?= $_SESSION['message'] ?></strong></p></div><?php 
    
  } ?>

  <form class="form-signin" method="POST" action="./index.php?action=login">
    <h2 class="form-signin-heading">Veuillez vous identifier</h2>
    <label for="login" class="sr-only">Identifiant</label>
    <input name="login" type="text" id="login" placeholder="Identifiant" class="form-control" required autofocus>
    
    <label for="pass" class="sr-only">Mot de passe</label>
    <input type="password" name="pass" id="pass" placeholder="Mot de passe" class="form-control" required>
    <!-- <div class="checkbox">
      <label>
        <input type="checkbox" value="remember-me" /> Se souvenir de moi
      </label>
    </div> -->
    <button class="btn btn-lg btn-primary btn-block" type="submit">Connexion</button>
  </form>
  <?php

}
else { ?>

      <p><a href="index.php?action=admin"><button>Accéder à la partie Admin</button></a></p>
      <p><a href="index.php?action=logout"><button>Déconnexion</button></a></p>
    <?php
    }
    ?>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>