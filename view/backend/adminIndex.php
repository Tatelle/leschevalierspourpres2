<?php $title = 'Admin - Accueil'; ?>

<?php ob_start(); 

if(isset($_SESSION['message_event'])){ ?>
	<p class="alert alert-success" id="message_erreur"><strong><?= $_SESSION['message_event'] ?></strong></p>
	<?php
	unset($_SESSION['message_event']);
} ?>

<h1>Bonjour <?= $_SESSION['login'] ?> !</h1>
<p><strong>Bienvenue dans la partie Administrateur</strong></p>

<div class="panel panel-success">
	<div class="panel-heading">
		<h2 class="panel-title">Gestion du Profil</h2>
	</div>
	<div class="panel-body">
		<a href="index.php?action=adminProfil"><button class="btn btn-success">Modifier mon profil</button></a>
	</div>
</div>

<div class="panel panel-success">
	<div class="panel-heading">
		<h2 class="panel-title">Gestion des profils</h2>
	</div>
	<div class="panel-body">
		<a href="index.php?action=adminNewUser"><button class="btn btn-success">Ajouter un utilisateur</button></a>
		<a href="index.php?action=adminAllUsers"><button class="btn btn-success">Voir tous les profils</button></a>
	</div>
</div>

<div class="panel panel-success">
	<div class="panel-heading">
		<h2 class="panel-title">Gestion des événements</h2>
	</div>
	<div class="panel-body">
		<a href="index.php?action=adminNewEvent"><button class="btn btn-success">Ajouter un événement</button></a>
		<a href="index.php?action=adminAllEvents"><button class="btn btn-success">Voir tous les événements à venir</button></a>
	</div>
</div>

<div class="panel panel-success">
	<div class="panel-heading">
		<h2 class="panel-title">Gestion des événements</h2>
	</div>
	<div class="panel-body">
		<a href="index.php?action=adminNewEvent"><button class="btn btn-success">Ajouter un événement</button></a>
		<a href="index.php?action=adminAllEvents"><button class="btn btn-success">Voir tous les événements à venir</button></a>
	</div>
</div>

<div class="panel panel-success">
	<div class="panel-heading">
		<h2 class="panel-title">Gestion des Prestations</h2>
	</div>
	<div class="panel-body">
		<a href="index.php?action=adminNewService"><button class="btn btn-success">Ajouter une prestation</button></a>
		<a href="index.php?action=adminAllServices"><button class="btn btn-success">Voir toutes les prestations</button></a>
	</div>
</div>

<?php
	$content = ob_get_clean(); 
?>

<?php require('template.php'); ?>