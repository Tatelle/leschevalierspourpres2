<?php
// Chargement des classes
use \LesChevaliersPourpes\Site\Model\AdminManager;
use \LesChevaliersPourpes\Site\Model\Manager;
use \LesChevaliersPourpes\Site\Model\ComingEventsManager;

require_once('model/AdminManager.php');
require_once('model/Manager.php');
require_once('model/ComingEventsManager.php');

function login()
{

    if(isset($_POST['login']) && isset($_POST['pass']) && $_POST['login'] != '' && $_POST['pass'] != ''){
        $login = htmlspecialchars($_POST['login']);
        $pass = htmlspecialchars($_POST['pass']);

        $loginManager = new AdminManager();
        $loginAdmin = $loginManager->getLogin($login);
        $isPasswordCorrect = password_verify($pass, $loginAdmin['password']);

        if ($loginAdmin === false){
            $_SESSION = array();
            session_destroy();
            $_SESSION['message'] = 'Mauvais identifiant ou mot de passe !';
        }
        else {
            if ($isPasswordCorrect) {                     
                $_SESSION['id'] = $loginAdmin['id'];
                $_SESSION['login'] = $login;
                header('Location: index.php?action=admin');
            }
            else {
                $_SESSION = array();
                session_destroy();
                $_SESSION['message'] = 'Mauvais identifiant ou mot de passe !';
            }
        }
    }
    else {
        $_SESSION['message'] = 'Vous devez remplir tous les champs';
    }
    require('view/frontend/signin.php');
}

function logout()
{
    $_SESSION = array();
    session_destroy();
    header('Location: ./index.php');
}

function adminIndex()
{    
    // $rights = checkRights();
    require('view/backend/adminIndex.php');
}

// Récupère les droits de l'utilisateur
function checkRights() 
{
    $adminManager = new AdminManager();
    $check = $adminManager->getUserRights($_SESSION['id']);

    return $check;
}

function adminProfil()
{
    require('view/backend/adminProfil.php');
}

function adminChangeLogin($newLogin)
{
    $adminManager = new AdminManager();
    $affectedLines = $adminManager->setNewlogin($_SESSION['id'], $newLogin);

    if ($affectedLines === false) {
        throw new Exception('Impossible de modifier l\'identifiant !');
    }
    else {
        $_SESSION['login'] = $newLogin;
        $_SESSION['message_profil']="L'identifiant a bien été modifié";
        header('Location: index.php?action=adminProfil');
    }
}

function adminCheckOldPassword($login, $passOld)
{
    $loginManager = new AdminManager();
    $loginAdmin = $loginManager->getLogin($login);
    $isPasswordCorrect = password_verify($passOld, $loginAdmin['password']);

    if ($loginAdmin === false){
        $_SESSION['message_erreur'] = 'Mot de passe incorrect ! Nous ne pouvons enregistrer votre nouveau mot de passe.';
    }
    else {
        if ($isPasswordCorrect) {
            return $isPasswordCorrect;
        }
        else {
            $_SESSION['message_erreur'] = 'Mot de passe incorrect ! Nous ne pouvons enregistrer votre nouveau mot de passe.';
            require('view/backend/adminProfil.php');
        }
    } 
}

function adminCryptPassword($password)
{
    $hash = crypt($password);

    return $hash;
}

function adminChangePassword($idlogin, $passNew)
{
    $passHash = adminCryptPassword($passNew);

    //echo $passHash;
    $adminManager = new AdminManager();
    $affectedLines = $adminManager->setNewPassword($idlogin, $passHash);

    if ($affectedLines === false) {
        throw new Exception('Impossible de modifier le mot de passe !');
    }
    else {
        $_SESSION['message_profil'] = 'Mot de passe modifié avec succès !';
        header('Location: index.php?action=adminProfil');
        //require('view/backend/adminProfil.php');
    }
}

//ajout d'un nouvel utilisateur
function adminNewUser()
{
    require('view/backend/adminNewUser.php');
}

//ajout d'un nouvel évènement
function adminNewEvent()
{
    require('view/backend/adminNewEvent.php');
}

function testDate( $valueDate )
{
    return preg_match( "#^\d{4}-\d{1,2}-\d{1,2}$#" , $valueDate);
}

// Ajout d'un nouvel utilisateur
function adminAddUser($login, $pass)
{

    $passHash = adminCryptPassword($pass);

    $adminManager = new AdminManager();
    $affectedLines = $adminManager->setNewUser($login, $passHash);

    if ($affectedLines === false) {
        throw new Exception('Impossible d\'enregistrer le nouvel utilisateur !');
    }
    else {
        $_SESSION['message_profil'] = 'Utilisateur ajouté avec succès !';
        header('Location: index.php?action=adminNewUser');
        //require('view/backend/adminProfil.php');
    }
}

// Ajout d'un nouvel évenement
function adminAddEvent($title, $eventContent, $place, $start_date, $end_date)
{
    $adminManager = new AdminManager();
    $affectedLines = $adminManager->setNewEvent($title, $eventContent, $place, $start_date, $end_date);

    if ($affectedLines === false) {
        throw new Exception('Impossible d\'ajouter le nouvel évenement !');
    }
    else {
        $_SESSION['message_event']="L'évenement a bien été ajouté";
        header('Location: index.php?action=admin');
    }
}

function adminAllEvents()
{
    $comingEvents = new ComingEventsManager();
    $comingEvents = $comingEvents->getComingEvents();

    require('view/backend/adminAllEvents.php');
}

function adminChangeEvent($eventId) {
    $eventManager = new ComingEventsManager();
    $event = $eventManager->getComingEvent($eventId);

    require('view/backend/adminChangeEvent.php');
}

function adminChangingEvent($eventId, $title, $eventContent, $place, $start_date, $end_date)
{
    //echo "coucou";
    $adminManager = new AdminManager();
    $affectedLines = $adminManager->setChangeEvent($eventId, $title, $eventContent, $place, $start_date, $end_date);

    if ($affectedLines === false) {
        throw new Exception('Impossible de modifier ce chapitre !');
    }
    else {
       $_SESSION['message_event']="Les modifications de l'évenement on été prises en compte !";
      header('Location: index.php?action=adminAllEvents');
    }    
}

function checkEvent($eventId) 
{
    $eventId = intval($eventId);
    $adminManager = new AdminManager();
    $check = $adminManager->getCheckEvent($eventId);

    return $check;
}

function adminDeleteEvent($eventId) 
{
    $adminManager = new AdminManager();
    $affectedLines = $adminManager->setDeleteEvent($eventId);

    if ($affectedLines === false) {
        throw new Exception('Impossible de supprimer cet évenement !');
    }
    else {
        $_SESSION['message_event'] = "L'évenement a bien été supprimé.";
        header('Location: index.php?action=adminAllEvents');
    }    
}

function adminNewService()
{
    require('view/backend/adminNewService.php');
}

function adminAddService($title, $serviceContent)
{
   echo 'coucou';

    // $adminManager = new AdminManager();
    // $affectedLines = $adminManager->setNewEvent($title, $eventContent, $place, $start_date, $end_date);

    // if ($affectedLines === false) {
    //     throw new Exception('Impossible d\'ajouter le nouvel évenement !');
    // }
    // else {
    //     $_SESSION['message_event']="L'évenement a bien été ajouté";
    //     header('Location: index.php?action=admin');
    // }
}

// function adminAllEvents()
// {
//     $comingEvents = new ComingEventsManager();
//     $comingEvents = $comingEvents->getComingEvents();

//     require('view/backend/adminAllEvents.php');
// }

// function adminChangeEvent($eventId) {
//     $eventManager = new ComingEventsManager();
//     $event = $eventManager->getComingEvent($eventId);

//     require('view/backend/adminChangeEvent.php');
// }

// function adminChangingEvent($eventId, $title, $eventContent, $place, $start_date, $end_date)
// {
//     //echo "coucou";
//     $adminManager = new AdminManager();
//     $affectedLines = $adminManager->setChangeEvent($eventId, $title, $eventContent, $place, $start_date, $end_date);

//     if ($affectedLines === false) {
//         throw new Exception('Impossible de modifier ce chapitre !');
//     }
//     else {
//        $_SESSION['message_event']="Les modifications de l'évenement on été prises en compte !";
//       header('Location: index.php?action=adminAllEvents');
//     }    
// }

// function checkEvent($eventId) 
// {
//     $eventId = intval($eventId);
//     $adminManager = new AdminManager();
//     $check = $adminManager->getCheckEvent($eventId);

//     return $check;
// }

// function adminDeleteEvent($eventId) 
// {
//     $adminManager = new AdminManager();
//     $affectedLines = $adminManager->setDeleteEvent($eventId);

//     if ($affectedLines === false) {
//         throw new Exception('Impossible de supprimer cet évenement !');
//     }
//     else {
//         $_SESSION['message_event'] = "L'évenement a bien été supprimé.";
//         header('Location: index.php?action=adminAllEvents');
//     }    
// }
