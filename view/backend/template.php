		<?php  include("./view/joint/head.php"); ?>
		
		<div class="container" id="content">
			<?php
			if (!isset($_SESSION['login'])) //On est dans la page de formulaire
			{ ?>
				<div class="alert alert-danger" id="non_connecte">
					<p><strong>Vous n'êtes pas autorisé à accéder à cette page !</strong> Merci de vous connecter...</p>
				</div>
				<p><a href="./index.php?action=signin"><button class="btn btn-info">Connexion</button></a></p>
			<?php
			}
			else 
			{ 
				echo $content;
			} ?>
   		</div>
       		
		<?php  include("./view/joint/footer.php"); ?>