<?php $title = 'Accueil - Les Chevaliers Pourpres'; ?>

<?php ob_start(); ?>

	<div id="blocks_accueil">

        <?php $data = $comingEvents->fetch(); ?>

        <a href="./index.php?action=coming_events">
            <div class="contenu_block">
                <h2>Prochain événement</h2>
                <?php if( $data != false ){ ?>
                    <h3><?= htmlspecialchars($data['title']); ?></h3>
                    <p class="lieu"><?= htmlspecialchars($data['place']) ?></p>
                    <?php
                    if( htmlspecialchars($data['start_date_fr']) == htmlspecialchars($data['end_date_fr']) ) { ?>
                        <p class="date">Le <?= htmlspecialchars($data['start_date_fr']); ?></p>
                        <?php 
                    }else{
                        ?>
                        <p class="date">Du <?= htmlspecialchars($data['start_date_fr']); ?> au <?= htmlspecialchars($data['end_date_fr']); ?></p>
                    <?php } 
                }else{ ?>
                    <p class="lieu">Aucune date disponible pour le moment</p>
                    <?php
                } ?>
            </div>
        </a>

        <?php $comingEvents->closeCursor(); ?>

        <a href="./index.php?action=contact">
            <div class="contenu_block">
                <h2>Contactez-nous</h2>
                <p>Vous avez une question ou souhaitez un devis pour une prestation ? N'hésitez-pas à nous contacter !</p>
            </div>
        </a>
        
    </div>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>