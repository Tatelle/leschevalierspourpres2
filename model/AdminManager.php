<?php 

namespace LesChevaliersPourpes\Site\Model;

require_once("model/Manager.php");
require_once 'model/Configuration.php';

class AdminManager extends Manager
{

    // vérification que l'utilisateur existe
    public function getLogin($login)
    {
        $sql = 'SELECT * FROM cp_users WHERE login = ?';
        $req = $this->executerRequete($sql, array($login));
        $loginAdmin = $req->fetch();

        return $loginAdmin;

        // $db = $this->dbConnect();   
        // $req = $db->prepare('SELECT * FROM cp_users WHERE login = :login');
        // $req->execute(array(
        //    'login' => $login));
        // $loginAdmin = $req->fetch();

        // return $loginAdmin;
    }

    // Ajout d'un nouvel utilisateur
    public function setNewUser($addlogin, $password)
    {
        echo $login.' '.$password;
        $sql = 'INSERT INTO `cp_users`(`login`, `password`) VALUES (?, ?)';
        $req = $this->executerRequete($sql, array($addlogin, $password));

        return $req;
        // $sql = 'UPDATE cp_users SET login = ? WHERE id = ?';
        // $req = $this->executerRequete($sql, array($newLogin, $idLogin));
        
        // return $req;
    }

    // Modification du nom d'utilisateur
    public function setNewlogin($idLogin, $newLogin)
    {
        
        $sql = 'UPDATE cp_users SET login = ? WHERE id = ?';
        $req = $this->executerRequete($sql, array($newLogin, $idLogin));
        
        return $req;

        // $db = $this->dbConnect();
        // $req = $db->prepare('INSERT INTO cp_coming_events(title, content, place, start_date, end_date) VALUES (?, ?, ?, ?, ?)');
        // $affectedLines = $req->execute(array($title, $eventContent, $place, $start_date, $end_date));
        // return $affectedLines;
    }

    // Modification du mot de passe
    public function setNewPassword($idlogin, $passNew)
    {

        $sql = 'UPDATE cp_users SET password = ? WHERE id = ?';
        $req = $this->executerRequete($sql, array($passNew, $idlogin));
        
        return $req;

        // $db = $this->dbConnect();
        // $req = $db->prepare('INSERT INTO cp_coming_events(title, content, place, start_date, end_date) VALUES (?, ?, ?, ?, ?)');
        // $affectedLines = $req->execute(array($title, $eventContent, $place, $start_date, $end_date));
        // return $affectedLines;
    }

    // Vérification des droits utilisateurs
    public function getUserRights($idLogin, $idRight=null)
    {
        $sql = 'SELECT * FROM cp_users_rights WHERE id_user = ?';
        if( $idRight != null ){
            $sql .= 'AND id_right = ?';
            $req = $this->executerRequete($sql, array($idLogin, $idRight));
        }else{
            $req = $this->executerRequete($sql, array($idLogin));
        }
        
        $loginAdmin = $req->fetch();

        return $loginAdmin;
    }

    // Ajout d'un nouvel évenement
    public function setNewEvent($title, $eventContent, $place, $start_date, $end_date)
    {
        
        $sql = 'INSERT INTO cp_coming_events(title, content, place, start_date, end_date) VALUES (?, ?, ?, ?, ?)';
        $req = $this->executerRequete($sql, array($title, $eventContent, $place, $start_date, $end_date));

        return $req;

        // $db = $this->dbConnect();
        // $req = $db->prepare('INSERT INTO cp_coming_events(title, content, place, start_date, end_date) VALUES (?, ?, ?, ?, ?)');
        // $affectedLines = $req->execute(array($title, $eventContent, $place, $start_date, $end_date));
        // return $affectedLines;
    }

    // Suppression d'un évenement
    public function setDeleteEvent($eventId)
    {
        $sql = 'DELETE FROM cp_coming_events WHERE id = ?';
        $req = $this->executerRequete($sql, array($eventId));
        
        if ($req === false) {
            throw new Exception('Impossible de supprimer l\'évenement !');
        }


        // $db = $this->dbConnect();
        // $req = $db->prepare('DELETE FROM cp_coming_events WHERE id = ?');
        // $deleteComment = $req->execute(array($eventId));

        // if ($deleteComment === false) {
        //     throw new Exception('Impossible de supprimer l\'évenement !');
        // }
    }

    // Récupération de tous les évenements
    public function getCheckEvent($eventId)
    {
        $sql = 'SELECT exists (SELECT * FROM cp_coming_events WHERE id = ?) AS event_exist';
        $req = $this->executerRequete($sql, array($eventId));
        $check = $req->fetch();

        return $check;


        // $sql = 'SELECT exists (SELECT * FROM cp_coming_events WHERE id = ?) AS event_exist';
        // $req = $this->executerRequete($sql, array($eventId));
        // $check = $req->fetch();

        // return $check;
    }

    // Changement d'un évenement
    public function setChangeEvent($eventId, $title, $eventContent, $place, $start_date, $end_date)
    {
        $sql = 'UPDATE cp_coming_events SET title = ?, content=?, place=?, start_date=?, end_date=? WHERE id = ?';
        $req = $this->executerRequete($sql, array($title, $eventContent, $place, $start_date, $end_date, $eventId));
        
        return $req;


        // $db = $this->dbConnect();
        // $req = $db->prepare('UPDATE cp_coming_events SET title = ?, content=?, place=?, start_date=?, end_date=? WHERE id = ?');
        // $affectedLines = $req->execute(array($title, $eventContent, $place, $start_date, $end_date, $eventId));

        // return $affectedLines;
    }

    // Récupération des prestations
    public function getServices()
    {
        $sql = 'SELECT * FROM cp_services ORDER BY id';
        $req = $this->executerRequete($sql);
        
        return $req;


        // $db = $this->dbConnect();
        // $req = $db->query('SELECT * FROM cp_services ORDER BY id');

        // return $req;
    }
}

