<!DOCTYPE html>
<html lang="fr">
    <head>
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    	<meta name="viewport" content="width=device-width, initial-scale=1" />
    	<meta name="description" content="'Les Chevaliers Pourpres' est une association de reconstitution médiévale ayant pour but de faire revivre la légende arthurienne." />

      <link rel="shortcut icon" href="public/images/favicon/favicon.ico" type="image/x-icon" />
      <link rel="icon" href="public/images/favicon/favicon.ico" type="image/x-icon" />

      <title><?= $title ?></title>

      <link href="vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
      <link href="vendor/bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet" />
      <link href="vendor/bootstrap/docs/examples/starter-template/starter-template.css" rel="stylesheet" /> 

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css" />
      <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" />

      <link href="public/css/style.css?4" rel="stylesheet" />

      <link href="https://fonts.googleapis.com/css?family=MedievalSharp%7CDancing+Script%7COpen+Sans%7CRaleway%7CTangerine" rel="stylesheet" />
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous" />

		<!-- <link rel="shortcut icon" href="public/images/favicon/favicon.ico" type="image/x-icon" />
		<link rel="icon" href="public/images/favicon/favicon.ico" type="image/x-icon" />

        <title><?= $title ?></title>

        <link href="vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link href="vendor/bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet" />
        <link href="vendor/bootstrap/docs/examples/starter-template/starter-template.css" rel="stylesheet" />	

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
		<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

	    <link href="public/css/style.css" rel="stylesheet" />

	    <link href="https://fonts.googleapis.com/css?family=MedievalSharp|Dancing+Script|Open+Sans|Raleway|Tangerine" rel="stylesheet" />
	    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous" /> -->
      <!-- <script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
        <script>
        tinymce.init({
          selector: '#chapterContent'
        });
      </script> -->
    </head>
        
    <body>
    	<nav class="navbar navbar-fixed-top">
      		<div class="container">
        		<div class="navbar-header">
          			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			            <span class="sr-only">Toggle navigation</span>
			            <span class="icon-bar"></span>
			            <span class="icon-bar"></span>
			            <span class="icon-bar"></span>
			         </button>
          			<a class="navbar-brand" href="index.php">
          				<img src="public/images/new_logo9.png" alt="Logo Les Chevaliers Pourpres" id="logo" class="hidden-xs" />
          				<p id="titre_principal">Les Chevaliers Pourpres</p>
          			</a>
        		</div>
		        <div id="navbar" class="navbar-collapse collapse">
		        	<ul class="nav navbar-right navbar-nav">
			            <li class="active"><a href="./index.php"><i class="fas fa-home"></i></a></li>
			            <li><a href="./index.php?action=association">L'Association</a></li>
			            <li><a href="./index.php?action=coming_events">Événements à venir</a></li>
			            <li><a href="./index.php?action=services">Nos Prestations</a></li>
			            <li><a href="./index.php?action=partners">Nos Partenaires</a></li>
			            <li><a href="./index.php?action=contact">Contact</a></li>
			        </ul>
		        </div>
      		</div>
   		</nav>