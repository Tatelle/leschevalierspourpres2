<?php $title = 'Ajouter un évenement - Admin'; ?>

<?php ob_start(); ?>

<h1>Edition d'un nouvel évenement</h1>

<p class="bouton_retour"><a href="index.php?action=admin"><button class="btn btn-default">Retour à l'accueil</button></a></p>

<form action="index.php?action=adminAddEvent" method="POST">
    <div class="form-group">
        <label for="title">Titre de l'évenement :</label>
        <input type="text" id="title" name="title"  class="form-control input-sm" required autofocus/>
    </div>
    <div class="form-group">
        <label for="place">Lieu de l'évenement :</label>
        <input type="text" id="place" name="place"  class="form-control input-sm" required/>
    </div>
    <div class="form-group">
        <label for="start_date">Date de début :</label>
        <input type="date" id="start_date" name="start_date"  class="form-control input-sm" placeholder="aaaa-mm-jj" required/>
    </div>
    <div class="form-group">
        <label for="end_date">Date de fin (si l'évenement est sur une journée, veuillez entrer la même date que la date de début) :</label>
        <input type="date" id="end_date" name="end_date"  class="form-control input-sm" placeholder="aaaa-mm-jj" required/>
    </div>
    <div class="form-group">
        <label for="eventContent">Description de l'évemenent :</label>
        <textarea  id="eventContent" name="eventContent" class="form-control" required></textarea>
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-success" value="Enregistrer l'évenement" />
    </div>
</form>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>