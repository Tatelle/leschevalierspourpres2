<?php $title = 'Erreur'; ?>

<?php ob_start(); ?>

<p class="alert alert-danger" id="message_erreur"><strong><?= 'Erreur : ' . $e->getMessage() ?></strong></p>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>