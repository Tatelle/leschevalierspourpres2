<?php $title = 'Modifier un évenement - Admin'; ?>

<?php ob_start(); ?>

<h1>Modification de l'évenement : <br/>
	<?= htmlspecialchars($event['title']) ?></h1>
    
<p class="bouton_retour"><a href="index.php?action=adminAllEvents" onclick="return(confirm('Etes-vous sûr de vouloir revenir à la page précédente sans sauvegarder ? les données modifiées seront perdues.'));"><button class="btn btn-default">Retour</button></a></p>

<form action="index.php?action=adminChangingEvent&amp;id=<?= $event['id'] ?>" method="POST">
    <div class="form-group">
        <label for="title">Titre de l'évenement :</label>
        <input type="text" id="title" name="title"  class="form-control input-sm" value="<?= htmlspecialchars($event['title']) ?>" required autofocus/>
    </div>
    <div class="form-group">
        <label for="place">Lieu de l'évenement :</label>
        <input type="text" id="place" name="place"  class="form-control input-sm" value="<?= htmlspecialchars($event['place']) ?>" required/>
    </div>
    <div class="form-group">
        <label for="start_date">Date de début :</label>
        <input type="date" id="start_date" name="start_date"  class="form-control input-sm" value="<?= htmlspecialchars($event['start_date']) ?>" required/>
    </div>
    <div class="form-group">
        <label for="end_date">Date de fin (si l'évenement est sur une journée, veuillez entrer la même date que la date de début) :</label>
        <input type="date" id="end_date" name="end_date" class="form-control input-sm" value="<?= htmlspecialchars($event['end_date']) ?>" required/>
    </div>
    <div class="form-group">
        <label for="eventContent">Description de l'évemenent :</label>
        <textarea  id="eventContent" name="eventContent" class="form-control" required><?= htmlspecialchars($event['content']) ?></textarea>
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-success" value="Sauvegarder" onclick="return(confirm('Etes-vous sûr de vouloir sauvegarder les modifications apportées ?'));" />
    </div>
</form>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>